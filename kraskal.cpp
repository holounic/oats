#include <iostream>
#include <vector>
using namespace std;

int main(void)
{
    int n, m; //n - вершины, m - ребра
    vector < pair <int, pair <int, int > > > g(m);
    int cost = 0;
    vector < pair <int, int> > res;
    vector <int> tree_id(n);
    
    sort(g.begin(), g.end());
    for (int i = 0; i<n; i++)
    {
        tree_id[i] = i;
    }
    for (int i = 0; i<m; i++)
    {
        int a = g[i].second.first;
        int b = g[i].second.second;
        int l = g[i].first;
        if (tree_id[a] != tree_id[b] )
        {
            int new_id = tree_id[a];
            int old_id = tree_id[b];
            tree_id[b] = tree_id[a];
            for (int j = 0; j<n; j++)
            {
                if (tree_id[j] == old_id) tree_id[j] = new_id;
            }
        }
    }
}