#include  <iostream>
#include <vector>
using namespace std;


int main(void)
{
    int n;
    cin>>n;
    vector <int> steps(n+1, 10000000);
    vector <int> papa(n+1, -1);
    steps[n] = 0;
    
    for (int i=n; i>1; i--)
    {
        int s = steps[i] + 1;
        if (!(i%3) && steps[i/3] > s)
        {
            steps[i/3] = s;
            papa[i/3] = i;
        }
        if(!(i%2) && steps[i/2] > s)
        {
            steps[i/2] = s;
            papa[i/2] = i;
        }
        if (steps[i-1]>s)
        {
            steps[i-1] = s;
            papa[i-1] = i;
        }
    }
    cout<<steps[1]<<endl;
    for (int i=1; i!=-1; i=papa[i])
    {
        cout<<i<<" ";
    }
}