#include <iostream>
#include <vector>
#include <queue>
using namespace std;

int main(void){
    int e; //amount of edges
    vector < vector<int> > graf(e);// graf as a vector of vectors with number of bounded edges
    queue <int> q;//queu for bfs
    vector <bool> visited(e);
    vector <int> d(e);

    int start;
    q.push(start);//push starting edge to the queue
    d[start] = 0;//starting edge is the reference point now
    visited[start] = true; //and we have visited this point
    vector <int> way; 

    while (!q.empty)//while our queue is not empty
    {
        int now = q.front(); //var now is the first member of the queue
        q.pop(); //throw this fucking member out of our cosy queue
        way.push_back(now);

        for (int i=0; i<graf[now].size(); i++)//checking neighbours 
        {
            if (visited[graf[now][i]]==false){
                visited[graf[now][i]] = true;
                d[graf[now][i]] = d[now] + 1;
                q.push(graf[now][i]);
            }
        }
    }
    for (int i=0; i<e; i++){
        cout << way[i]<<" "<<d[way[i]]<<endl;
    }
}