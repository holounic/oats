#include <cmath>
#include <iostream>
using namespace std;
int main(void) {
  int n;
  cin >> n;
  int a[n];
  int res[n];
  for (int i=0; i<n; i++){
      a[i]=res[i]=0;
  }

  for (int i = 0; i < n; i++) {
    cin >> a[i];
  }

  res[0] = 0;
  res[1] = abs(a[1] - a[0]);
  for (int i = 2; i < n; i++) {
    int nrm = abs(a[i] - a[i - 1]) + res[i - 1];
    int mega = 3 * (abs(a[i] - a[i - 2])) + res[i - 2];
    if (nrm > mega) {
      res[i] = mega;
    } else {
      res[i] = nrm;
    }
  }

  cout << res[n - 1];
}
