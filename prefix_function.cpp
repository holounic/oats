#include <iostream>
#include <vector>
using namespace std;

vector <int> prefix_function(string s)  {
    int n = (int) s.length();
    vector<int> pi(n);
    for (int i = 1; i<n; i++)   {
        int j = pi[i-1];
        while (j>0 && s[i]!=s[j])
            j = pi[j-1];
            if (s[j] == s[i]) ++j;
            pi[i] = j;
    }
    return pi;
    //на самом деле нам нужен только pi[n-1]
}
int main(void)  {
    string s;
    getline(cin, s);
    vector <int> res = prefix_function(s);
    for (int i = 0; i<res.size(); i++)  
        cout << res[i] << endl;
}