#include <iostream>
#include <vector>
#include <stack>
using namespace std;

int main(void){
    int e; //amount of edges
    vector < vector<int> > graf(e);// graf as a vector of vectors with numbers of bounded edges
    stack <int> s;//stack for dfs
    vector <bool> visited(e);
    vector <int> d(e);

    int start;
    s.push(start);//push starting edge to the stack
    d[start] = 0;//starting edge is the reference point now
    visited[start] = true; //and we have visited this point
    vector <int> way; 
    while (!s.empty())
    {
        int now = s.top();//var now eqls the top member of the stack
        s.pop();//throw this garbage out
        way.push_back(now);
        for (int i=0; i<graf[now].size(); i++){
            if (visited[graf[now][i]]==false){
                visited[graf[now][i]]=true;//yes, we have been here
                s.push[graf[now][i]];//add edge to the stack
            }
        }
    }
}