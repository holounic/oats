#include <iostream>
#include <vector>
using namespace std;
vector <int> a;

void quick_sort(int cur_start, int cur_finish)
{
    int start = cur_start, finish = cur_finish;
    int template_val = a[(start+finish)/2];
    while (start <= finish)
    {
        while (a[start] < template_val)
            start++;
        while (a[finish] > template_val)
            finish--;
        if (start <=finish)
            swap(a[start++], a[finish--]);
    }
    if (cur_start < finish) 
        quick_sort(cur_start, finish);
    if (cur_finish > start)
        quick_sort(start, cur_finish);
}

int main(void)
{
    int n;
    cin >> n;
    a.resize(n);
    for (int i = 0; i<n; i++)
    {
        cin >> a[i];
    }

    quick_sort(0, n-1);
    for(int i =0; i<n; i++)
    {
        cout << a[i];
    }
}
