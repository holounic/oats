#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
using namespace std;
vector<vector <pair <int, int> > > g;
vector <vector <int> >st;
vector <bool> used;
int finish;
int dfs(int edge, int cmin)//cmin - пропускная способнотсь в данном подпотоке
{
    if (edge == finish) return cmin;
    used[edge] = true;
    for (int i = 0; i<g[edge].size(); i++)
    {
        int next = g[edge][i].first, next_c = g[edge][i].second, next_f = st[edge][i];
        if (!used[next] && next_f < next_c )
            {
                int delta = dfs(edge, min(cmin, next_c - next_f));
                if (delta > 0)
                {
                    st[edge][i] +=delta;
                    g[edge][i].second -=delta;
                    return delta;
                }
            }
    }
}
//выводим st[edge][finish]
//начинаем с edge = исток
