#include <iostream>
#include <vector>
using namespace std;
//сейчас мы пишем топ сорт с условием, что граф не содержит циклов
int N; // количество вершин
vector <vector <int> > g;
vector <bool> used;
vector <int> srt; 
 
void bfs(int v)
{
    used[v] = true;
    for (int i=0; i<g[v].size(); i++)
    {
        if (!used[g[v][i]])
            bfs(g[v][i]);
    }
    srt.push_back(v);
} 

void topSort()
{
    for (int i=1; i<=N; i++)
    {
        if (!used[i]) //таким образом мы пройдем все вершины
            bfs(i);
    }
    reverse(srt.begin(), srt.end());
}
