#include <iostream>
#include <vector>

int N;
std::vector <std::vector <int> > g;
std::vector <int> colour;
std::vector <int> parents;
std::vector <int> cycle;
int cycle_start = -1, cycle_end = -1;

bool dfs(int v)
{
    int now = v;
    colour[v] = 1;
    for (int i=0; i<g[now].size(); i++)
    {   
        int to = g[now][i];
        if (colour[to] == 0)
        {
            parents[to] = now;
            if (dfs(to)) return true;
        } 
        else if (colour[to] == 1)
        {
            cycle_start = to;
            cycle_end = now;
            return true;
        }
    }
    colour[now] = 2;
    return false;
}

bool look_for_cycle()
{
    for (int i=1; i<=N; i++)
    {   
        if (dfs(i)) 
            break;
    }
    if (cycle_start==-1) return false;
    else {
        return true;
    }
}

int main(void)
{
    int M;
    std::cin>>N>>M;
    g.resize(N+1);
    parents.resize(N+1);
    colour.assign(N+1, 0);
    int a, b;
    for (int i=0; i<M; i++)
    {   
        std::cin>>a>>b;
        g[b].push_back(a);
    }
    if (!look_for_cycle())
    {
        std::cout<<"NO";
    } else {
        std::cout<<"YES"<<std::endl;
        for (int i=cycle_end; i!=cycle_start && i!=0; i=parents[i])
        {
            std::cout<< i << " ";
        }
        std::cout<<cycle_start<<std::endl;
    }

}